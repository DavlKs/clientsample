package org.example;

import com.example.CardServerGrpc;
import com.example.Client;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;


import javax.net.ssl.SSLException;

public class SimpleClient {

    public static void main(String[] args) throws SSLException {

        ManagedChannel managedChannel = ManagedChannelBuilder
                .forAddress("localhost", 8085)
                .usePlaintext()
                .build();

        CardServerGrpc.CardServerBlockingStub stub = CardServerGrpc.newBlockingStub(managedChannel);

        Client.GetCardNumberRequest cardNumberRequest = Client.GetCardNumberRequest.newBuilder()
                .setCardId(2)
                .build();

        Client.GetCardNumberResponse cardNumberResponse = stub.getCardNumber(cardNumberRequest);

        System.out.println(cardNumberResponse);

        managedChannel.shutdownNow();
    }
}
